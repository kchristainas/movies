# README

This app "MovieRama" uses:

* Ruby 2.3.4

* Rails 5.1.4

* Postgresql (pg '0.21.0')

Database creation:

* rake db:create

Database initialization:

* rake db:seed (demo content and users are created)

Deployment instructions:

* git heroku push master
