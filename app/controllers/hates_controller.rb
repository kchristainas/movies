class HatesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_hate, only: [:destroy]

  # POST /hates
  # POST /hates.json
  def create
    # Stops someone who wants to vote for his movies.
    redirect_to movies_path, alert: 'Voting for same user is not allowed' and return if current_user.movie_ids.include?(hate_params[:movie_id].to_i)

    @hate = Hate.find_or_initialize_by(movie_id: hate_params[:movie_id], user_id: current_user.id)

    @hate.attributes.merge(hate_params)

    respond_to do |format|
      if @hate.save
        format.html { redirect_to movies_path, notice: 'Hate was successfully created.' }
        format.json { render :show, status: :created, location: @hate }
      else
        format.html { render :new }
        format.json { render json: @hate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hates/1
  # DELETE /hates/1.json
  def destroy
    respond_to do |format|
      if @hate && @hate.destroy
        format.html { redirect_to movies_url, notice: 'Hate was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { redirect_to movies_url, notice: 'Hate could not be destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hate
      @hate = current_user.hates.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hate_params
      params.require(:hate).permit(:movie_id)
    end
end
