class LikesController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  before_action :set_like, only: [:destroy]

  # POST /likes
  # POST /likes.json
  def create
    # Stops someone who wants to vote for his movies.
    redirect_to movies_path, alert: 'Voting for same user is not allowed' and return if current_user.movie_ids.include?(like_params[:movie_id].to_i)

    @like = Like.find_or_initialize_by(movie_id: like_params[:movie_id], user_id: current_user.id)

    @like.attributes.merge(like_params)

    respond_to do |format|
      if @like.save
        format.html { redirect_to movies_path, notice: 'Like was successfully created.' }
        format.json { render :show, status: :created, location: @like }
      else
        format.html { render :new }
        format.json { render json: @like.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /likes/1
  # DELETE /likes/1.json
  def destroy
    respond_to do |format|
      if @like && @like.destroy
        format.html { redirect_to movies_url, notice: 'Like was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { redirect_to movies_url, alert: 'Like could not be destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_like
      @like = current_user.likes.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def like_params
      params.require(:like).permit(:movie_id)
    end
end
