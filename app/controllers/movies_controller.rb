class MoviesController < ApplicationController
  before_action :authenticate_user!, only: [:show, :new, :create]
  before_action :set_movie, only: [:show]

  # GET /movies
  # GET /movies.json
  def index
    sort_scope  = "ordered_by_" << (Movie::SORT_OPTIONS.include?(params[:sort]) ? params[:sort] : "likes")
    user_scope  = params[:user_id]

    @movies     = Movie.user_scoped(user_scope).send(sort_scope)
  end

  # GET /movies/1
  # GET /movies/1.json
  def show
  end

  # GET /movies/new
  def new
    @movie = Movie.new
  end

  # POST /movies
  # POST /movies.json
  def create
    @movie = Movie.new(movie_params)

    respond_to do |format|
      if @movie.save
        format.html { redirect_to @movie, notice: 'Movie was successfully created.' }
        format.json { render :show, status: :created, location: @movie }
      else
        format.html { render :new }
        format.json { render json: @movie.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_movie
      @movie = Movie.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def movie_params
      params.require(:movie).permit(:title, :description, :user_id)
    end
end
