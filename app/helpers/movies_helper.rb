module MoviesHelper
  # Returns user's first_name and last_name or 'You'.
  def get_user_nickname(movie)
    return '' unless movie.class == Movie
    movie.user == current_user ? 'You' : movie.user.nickname
  end
end
