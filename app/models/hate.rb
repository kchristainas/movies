class Hate < Vote
  after_save :destroy_movie_like
  belongs_to :movie, :counter_cache => true

  # Destroys any like of same user to same movie.
  # a user cannot both like and hate a movie the same time.
  #
  def destroy_movie_like
    Like.find_by(user: user, movie: movie).try(:destroy)
  end
end
