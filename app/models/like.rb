class Like < Vote
  after_save :destroy_movie_hate
  belongs_to :movie, :counter_cache => true

  def destroy_movie_hate
    Hate.find_by(user: user, movie: movie).try(:destroy)
  end
end