class Movie < ApplicationRecord
  has_many :likes
  has_many :hates
  belongs_to :user
  has_many :liking_users, through: :likes, source: :user
  has_many :hating_users, through: :hates, source: :user

  scope :ordered_by_likes,  -> { order(likes_count: :desc, hates_count: :asc) }
  scope :ordered_by_hates,  -> { order(hates_count: :desc, likes_count: :asc) }
  scope :ordered_by_date,   -> { order(created_at: :desc) }
  scope :user_scoped,       -> (user_id) { where(user_id: user_id) if user_id.present? }

  SORT_OPTIONS = ['likes', 'hates', 'date']

  def has_votes?
    !(likes_count + hates_count).zero?
  end
end
