class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :movies
  has_many :likes
  has_many :hates
  has_many :liked_movies, through: :likes, source: :movie
  has_many :hated_movies, through: :hates, source: :movie

  validates :first_name, presence: true
  validates :last_name, presence: true

  def nickname
    @nickname ||= [first_name, last_name].join(" ")
  end

  def get_liked_movies_ids
    @liked_movies_ids ||= liked_movies.map(&:id)
  end

  def get_hated_movies_ids
    @hated_movies_ids ||= hated_movies.map(&:id)
  end

  def likes_movie?(movie)
    get_liked_movies_ids.include?(movie.id)
  end

  def hates_movie?(movie)
    get_hated_movies_ids.include?(movie.id)
  end
end
