Rails.application.routes.draw do
  root to: "movies#index"

  devise_for :users

  resources :likes, only: [:create, :destroy]
  resources :hates, only: [:create, :destroy]
  resources :movies, only: [:new, :create, :show, :index]

  resources :users do
    resources :movies
  end

end
