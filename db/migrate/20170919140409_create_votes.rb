class CreateVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :votes do |t|
      t.string :type
      t.references :user, foreign_key: true
      t.references :movie, foreign_key: true

      t.timestamps
    end
    add_index :votes, :type
  end
end
