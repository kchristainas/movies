class AddVotesCountToMovie < ActiveRecord::Migration[5.1]
  def change
    add_column :movies, :likes_count, :integer, default: 0
    add_column :movies, :hates_count, :integer, default: 0

    add_index :movies, :likes_count
    add_index :movies, :hates_count
  end
end
