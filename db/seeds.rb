# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


users = User.create([
  { email: 'user1@movierama.com', first_name: 'User', last_name: 'One', password: '123456', password_confirmation: '123456' },
  { email: 'user2@movierama.com', first_name: 'User', last_name: 'Two', password: '123456', password_confirmation: '123456' },
  { email: 'user3@movierama.com', first_name: 'User', last_name: 'Three', password: '123456', password_confirmation: '123456' },
  { email: 'user4@movierama.com', first_name: 'User', last_name: 'Four', password: '123456', password_confirmation: '123456' },
  { email: 'user5@movierama.com', first_name: 'User', last_name: 'Five', password: '123456', password_confirmation: '123456' },
  { email: 'user6@movierama.com', first_name: 'User', last_name: 'Six', password: '123456', password_confirmation: '123456' },
  { email: 'user7@movierama.com', first_name: 'User', last_name: 'Seven', password: '123456', password_confirmation: '123456' },
  { email: 'user8@movierama.com', first_name: 'User', last_name: 'Eight', password: '123456', password_confirmation: '123456' },
  { email: 'user9@movierama.com', first_name: 'User', last_name: 'Nine', password: '123456', password_confirmation: '123456' },
  { email: 'user10@movierama.com', first_name: 'User', last_name: 'Ten', password: '123456', password_confirmation: '123456' }
  ])

movies = Movie.create([
  { title: 'Star Wars', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', user: User.find(1), created_at: Time.now - 8.hours },
  { title: 'Lord of the Rings', description: 'Donec metus magna, convallis sit amet tincidunt ac, consectetur non eros.', user: User.find(1), created_at: Time.now - 6.hours },
  { title: 'The Empire strikes back', description: 'Praesent accumsan nec orci id sodales. Praesent vestibulum lorem at accumsan lobortis.', user: User.find(1), created_at: Time.now - 4.hours },
  { title: 'Sunshine', description: 'Fusce posuere, sapien laoreet porttitor vehicula, lectus nibh feugiat est, quis lobortis purus metus non mauris.', user: User.find(2), created_at: Time.now - 2.hours }
  ])

likes = Like.create([
  { movie: Movie.find(1), user: User.find(2) },
  { movie: Movie.find(1), user: User.find(3) },
  { movie: Movie.find(2), user: User.find(2) },
  { movie: Movie.find(2), user: User.find(3) },
  { movie: Movie.find(2), user: User.find(4) },
  { movie: Movie.find(3), user: User.find(2) },
  { movie: Movie.find(3), user: User.find(3) }
  ])

hates = Hate.create([
  { movie: Movie.find(1), user: User.find(5) },
  { movie: Movie.find(1), user: User.find(6) },
  { movie: Movie.find(2), user: User.find(5) },
  { movie: Movie.find(2), user: User.find(6) },
  { movie: Movie.find(3), user: User.find(8) },
  { movie: Movie.find(3), user: User.find(9) },
  { movie: Movie.find(3), user: User.find(10) }
  ])