require 'test_helper'

class HatesControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get hates_create_url
    assert_response :success
  end

  test "should get destroy" do
    get hates_destroy_url
    assert_response :success
  end

end
